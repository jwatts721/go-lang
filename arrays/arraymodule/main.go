package main

import (
	"fmt"
)

func arrays() {
	var nums[3] int
	nums[0] = 1
	nums[1] = 2
	nums[2] = 3

	fmt.Println(nums)

	favNums := [4] int {50, 25, 30, 33}
	fmt.Println(favNums[3])
}

func main() {
	arrays()
}