package main

import "testing"

func testOutput(t *testing.T) {
	expected := "Testing 123"
	if got := print(); got != expected {
		t.Errorf("print() = %q, want %q", got, expected)
	}
}