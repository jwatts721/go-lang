package main

import (
	"fmt"
)

func numbers() {
	var a int = 4
	var b, c int
	b = 5
	c = 10
	fmt.Println(a)
	fmt.Println(b + c)

	var d float64 = 9.14
	fmt.Println(d)
}

func strings() {
	var s string = "This is a string s"
	fmt.Println(s)
}

func shorthand() {
	a := 9
	b := "golang"
	c := 4.17
	d := false
	
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(d)
}

func main() {
	numbers()
	strings()
	shorthand()
}