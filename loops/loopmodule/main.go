package main

import (
	"fmt"
)

func loops() {
	for i := 0; i < 10; i++ {
		fmt.Println(i)
	}
}

func main() {
	loops()
}