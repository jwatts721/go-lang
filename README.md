# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* GoLang Tutorial
* [GoLang Docs](https://golangdocs.com)
* [Go Hello World Program](https://dev.to/chefgs/go-hello-world-program-17pk#introduction)
* [Learn](https://learn-golang.org)

### Quick Reference
* go version
* cd src/
* go mod init <module_directory>
* cd <module_directory>
* go run main.go
* go build
* go test