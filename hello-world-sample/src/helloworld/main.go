package main

import "fmt"

func print() string {
	str := "Hello, World!"
	return str
}

func main() {
	val := print()
	fmt.Println(val)
}